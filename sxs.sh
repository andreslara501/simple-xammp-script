#!/bin/bash
# Format of command: ./xampp.sh new.website.name.com projectDir path/to/project/Dir


# Number of expected arguments
EXPECTED_ARGS=2

function create_symbolic_link {
	sudo ln -s $1 /opt/lampp/htdocs/$2
}


function modify_vhosts {
	sudo printf "\n<VirtualHost *:80>\n\tDocumentRoot \"/opt/lampp/htdocs/$2\" \n\tServerName $1 \n</VirtualHost>" >> /opt/lampp/etc/extra/httpd-vhosts.conf
	sudo printf "\n127.0.0.1           $1" >> /etc/hosts
}


clear
if [ $# -ne $EXPECTED_ARGS ]; then
	echo "Inputed wrong number of arguments"
else
	#Modify vhost file with given url
	modify_vhosts $1 $2
    echo "Creado con éxito http://$1 en la carpeta /opt/lampp/htdocs/$2"
    sudo /opt/lampp/lampp restart
fi
